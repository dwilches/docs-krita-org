msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:40+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/vector_library.rst:1
msgid "Overview of the vector library docker."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
#: ../../reference_manual/dockers/vector_library.rst:15
msgid "Vector Library"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "SVG Symbols"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "Reusable Vector Shapes"
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:19
msgid ""
"The Vector Library Docker loads the symbol libraries in SVG files, when "
"those SVG files are put into the \"symbols\" folder in the resource folder :"
"menuselection:`Settings --> Manage Resources --> Open Resource Folder`."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:21
msgid ""
"The vector symbols can then be dragged and dropped onto the canvas, allowing "
"you to quickly use complicated images."
msgstr ""

#: ../../reference_manual/dockers/vector_library.rst:23
msgid ""
"Currently, you cannot make symbol libraries with Krita yet, but you can make "
"them by hand, as well as use Inkscape to make them. Thankfully, there's "
"quite a few svg symbol libraries out there already!"
msgstr ""
