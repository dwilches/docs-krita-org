# translation of docs_krita_org_reference_manual___stroke_selection.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___stroke_selection\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-03-13 12:33+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:1
#, fuzzy
#| msgid ".. image:: images/en/Stroke_Selection_4.png"
msgid ".. image:: images/Stroke_Selection_4.png"
msgstr ".. image:: images/en/Stroke_Selection_4.png"

#: ../../reference_manual/stroke_selection.rst:1
msgid "How to use the stroke selection command in Krita."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:10
#, fuzzy
#| msgid "Stroke Selection"
msgid "Selection"
msgstr "Výber ťahu"

#: ../../reference_manual/stroke_selection.rst:10
msgid "Stroke"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:15
msgid "Stroke Selection"
msgstr "Výber ťahu"

#: ../../reference_manual/stroke_selection.rst:17
msgid ""
"Sometimes, you want to add an even border around a selection. Stroke "
"Selection allows you to do this. It's under :menuselection:`Edit --> Stroke "
"Selection`."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:19
msgid "First make a selection and call up the menu:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:22
#, fuzzy
#| msgid ".. image:: images/en/Krita_stroke_selection_1.png"
msgid ".. image:: images/Krita_stroke_selection_1.png"
msgstr ".. image:: images/en/Krita_stroke_selection_1.png"

#: ../../reference_manual/stroke_selection.rst:23
msgid ""
"The main options are about using the current brush, or lining the selection "
"with an even line. You can use the current foreground color, the background "
"color or a custom color."
msgstr ""

#: ../../reference_manual/stroke_selection.rst:25
msgid "Using the current brush allows you to use textured brushes:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:28
#, fuzzy
#| msgid ".. image:: images/en/Stroke_selection_2.png"
msgid ".. image:: images/Stroke_selection_2.png"
msgstr ".. image:: images/en/Stroke_selection_2.png"

#: ../../reference_manual/stroke_selection.rst:29
msgid ""
"Lining the selection also allows you to set the background color, on top of "
"the line width in pixels or inches:"
msgstr ""

#: ../../reference_manual/stroke_selection.rst:32
#, fuzzy
#| msgid ".. image:: images/en/Krita_stroke_selection_3.png"
msgid ".. image:: images/Krita_stroke_selection_3.png"
msgstr ".. image:: images/en/Krita_stroke_selection_3.png"

#: ../../reference_manual/stroke_selection.rst:33
msgid "This creates nice silhouettes:"
msgstr ""
