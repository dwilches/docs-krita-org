# translation of docs_krita_org_reference_manual___tools___color_selector.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___color_selector\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 13:07+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/tools/color_selector.rst:1
msgid "Krita's color selector tool reference."
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Colors"
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Eyedropper"
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:12
#, fuzzy
#| msgid "Color Selector Tool"
msgid "Color Selector"
msgstr "Nástroj na výber farby"

#: ../../reference_manual/tools/color_selector.rst:17
msgid "Color Selector Tool"
msgstr "Nástroj na výber farby"

#: ../../reference_manual/tools/color_selector.rst:20
msgid ""
"This tool allows you to choose a point from the canvas and make the color of "
"that point the active foreground color. When a painting or drawing tool is "
"selected the Color Picker tool can also be quickly accessed by pressing the :"
"kbd:`Ctrl` key."
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:23
msgid ".. image:: images/tools/Color_Dropper_Tool_Options.png"
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:24
msgid ""
"There are several options shown in the :guilabel:`Tool Options` docker when "
"the :guilabel:`Color Picker` tool is active:"
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:26
msgid ""
"The first drop-down box allows you to select whether you want to sample from "
"all visible layers or only the active layer. You can choose to have your "
"selection update the current foreground color, to be added into a color "
"palette, or to do both."
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:30
msgid ""
"The middle section contains a few properties that change how the Color "
"Picker picks up color; you can set a :guilabel:`Radius`, which will average "
"the colors in the area around the cursor, and you can now also set a :"
"guilabel:`Blend` percentage, which controls how much color is \"soaked up\" "
"and mixed in with your current color. Read :ref:`mixing_colors` for "
"information about how the Color Picker's blend option can be used as a tool "
"for off-canvas color mixing."
msgstr ""

#: ../../reference_manual/tools/color_selector.rst:32
msgid ""
"At the very bottom is the Info Box, which displays per-channel data about "
"your most recently picked color. Color data can be shown as 8-bit numbers or "
"percentages."
msgstr ""
