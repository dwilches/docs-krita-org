# Translation of docs_krita_org_reference_manual___brushes___brush_engines___grid_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___grid_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Mix with background color"
msgstr "Змішування із кольором тла"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:1
msgid "The Grid Brush Engine manual page."
msgstr "Розділ підручника щодо рушія пензлів ґратки."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:17
msgid "Grid Brush Engine"
msgstr "Рушій пензлів ґратки"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
msgid "Grid"
msgstr "Ґратка"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:21
msgid ".. image:: images/icons/gridbrush.svg"
msgstr ".. image:: images/icons/gridbrush.svg"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:22
msgid ""
"The grid brush engine draws shapes on a grid. It helps you produce retro and "
"halftone effects."
msgstr ""
"Рушій пензлів ґратки малює форми на ґратці. Корисний для створення ретро-"
"ефектів та ефектів півтонів."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:24
msgid ""
"If you're looking to setup a grid for snapping, head to :ref:"
"`grids_and_guides_docker`."
msgstr ""
"Якщо вам потрібна ґратка для прилипання, зверніться до розділу :ref:"
"`grids_and_guides_docker`."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:27
msgid "Options"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:29
msgid ":ref:`option_size_grid`"
msgstr ":ref:`option_size_grid`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:30
msgid ":ref:`option_particle_type`"
msgstr ":ref:`option_particle_type`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:31
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:32
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:33
msgid ":ref:`option_color_grid`"
msgstr ":ref:`option_color_grid`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:38
msgid "Brush Size"
msgstr "Розмір пензля"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:40
msgid "Grid Width"
msgstr "Ширина ґратки"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:41
msgid "Width of the cursor area."
msgstr "Ширина області вказівника."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:42
msgid "Grid Height"
msgstr "Висота ґратки"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:43
msgid "Height of the cursor area."
msgstr "Висота області вказівника."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:44
msgid "Division"
msgstr "Ділення"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:45
msgid ""
"Subdivides the cursor area and uses the resulting area to draw the particles."
msgstr ""
"Ділить ділянку під курсором і використовує отриману ділянку для малювання "
"часток."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:46
msgid "Division by pressure"
msgstr "Поділ за натиском"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:47
msgid ""
"The more you press, the more subdivisions. Uses Division as the finest "
"subdivision possible."
msgstr ""
"Чим сильнішим є тиск, тим більшою буде кількість ділянок поділу. "
"Використовує значення «Ділення» як найменший можливий розмір ділянки поділу."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:48
msgid "Scale"
msgstr "Масштабувати"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:49
msgid "Scales up the area."
msgstr "Масштабує область."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:50
msgid "Vertical Border"
msgstr "Межа по вертикалі"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:51
msgid ""
"Forces vertical borders in the particle space, between which the particle "
"needs to squeeze itself."
msgstr ""
"Примусово визначає вертикальні межі простору для частки, у які програма має "
"вмістити цю частку."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:52
msgid "Horizontal Border"
msgstr "Межа по горизонталі"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:53
msgid ""
"Forces a horizontal borders in the particle space, between which the "
"particle needs to squeeze itself."
msgstr ""
"Примусово визначає горизонтальні межі простору для частки, у які програма "
"має вмістити цю частку."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:55
msgid "Jitter Borders"
msgstr "Нестійкі межі"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:55
msgid "Randomizes the border values with the Border values given as maximums."
msgstr "Створює випадкові межі максимальних значень для параметрів обмеження."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:60
msgid "Particle Type"
msgstr "Тип частки"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:62
msgid "Decides the shape of the particle."
msgstr "Визначає форму частки."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:64
msgid "Ellipse"
msgstr "Еліпс"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:65
msgid "Fills the area with an ellipse."
msgstr "Заповнює область еліпсом."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:66
msgid "Rectangle"
msgstr "Прямокутник"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:67
msgid "Fills the area."
msgstr "Заповнює область."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:68
msgid "Line"
msgstr "Лінія"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:69
msgid ""
"Draws lines from the lower left to the upper right corner of the particle."
msgstr "Малює ліній з нижнього лівого до верхнього правого кута частки."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:70
msgid "Pixel"
msgstr "Піксель"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:71
msgid "Looks like an aliased line on high resolutions."
msgstr "Виглядає як згладжена лінія за високих роздільних здатностей."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:73
msgid "Anti-aliased Pixel"
msgstr "Згладжування пікселів"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:73
msgid "Fills the area with little polygons."
msgstr "Заповнює ділянку маленькими багатокутниками."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:78
msgid "Color Options"
msgstr "Параметри кольорів"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:80
msgid "Random HSV"
msgstr "Випадкова HSV"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:81
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""
"Визначає випадкові значення відтінку насиченості і значення, де дисперсія "
"визначається повзунками. Чим більшим є значення на повзунку, тим сильніше "
"колір відхилятиметься від кольору переднього плану. Відхилення може "
"відбуватися за або проти годинникової стрілки."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:82
msgid "Random Opacity"
msgstr "Випадкова непрозорість"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:83
msgid "Randomizes the opacity."
msgstr "Робить непрозорість випадковою."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:84
msgid "Color Per Particle"
msgstr "Колір на частку"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:85
msgid "Has the color options be per particle instead of area."
msgstr "Містить параметри кольору для окремих часток, а не області."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:86
msgid "Sample Input Layer"
msgstr "Шар зразка"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:87
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""
"Визначає, що еталонним для кольорів є підлеглий шар, а не колір переднього "
"плану."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:88
msgid "Fill Background"
msgstr "Заповнення тла"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:89
msgid "Fills the area before drawing the particles with the background color."
msgstr "Заповнює ділянку кольором тла до малювання часток."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:91
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
"Надає частці випадкового кольору між кольором переднього плану, вказаного "
"кольору або випадковим кольором та кольором тла."
