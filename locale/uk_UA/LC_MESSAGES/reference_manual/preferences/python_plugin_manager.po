# Translation of docs_krita_org_reference_manual___preferences___python_plugin_manager.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___python_plugin_manager\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/preferences/python_plugin_manager.rst:None
msgid ""
".. image:: images/preferences/Krita_4_0_preferences_python_plugin_manager.png"
msgstr ""
".. image:: images/preferences/Krita_4_0_preferences_python_plugin_manager.png"

#: ../../reference_manual/preferences/python_plugin_manager.rst:1
msgid "Python Plugin Manager in Krita."
msgstr "Керування додатками Python у Krita."

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Python Scripting"
msgstr "Сценарії на Python"

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Python"
msgstr "Python"

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Scripts"
msgstr "Скрипти"

#: ../../reference_manual/preferences/python_plugin_manager.rst:15
msgid "Python Plugin Manager"
msgstr "Керування додатками Python"

#: ../../reference_manual/preferences/python_plugin_manager.rst:17
msgid "This is part of Krita's python support."
msgstr "Це частина підтримки Python у Krita."

#: ../../reference_manual/preferences/python_plugin_manager.rst:23
msgid ""
"The python plugin manager can be accessed from :menuselection:`Settings --> "
"Configure Krita --> Python Plugin Manager`. It allows you decide which of "
"the Python Plugins are active."
msgstr ""
"Доступ до засобу керування додатками Python можна отримати за допомогою "
"пункту меню :menuselection:`Параметри --> Налаштувати Krita --> Керування "
"додатками Python`. За допомогою цього засобу ви можете визначити, які з "
"додатків мовою Python будуть активними."

#: ../../reference_manual/preferences/python_plugin_manager.rst:25
msgid ""
"It will show you a list of python plugins Krita has found, as well as their "
"description. By default, Python Plugins are disabled, because many python "
"scripts are autostarted, so this ensures only the ones you want to run are "
"being run."
msgstr ""
"Програма покаже вам список виявлених Krita додатків мовою Python, а також "
"короткі описи цих додатків. Типово, додатки Python вимкнено, оскільки багато "
"сценаріїв Python запускаються автоматично. Через це розробники вирішили, що "
"користувач має сам визначитися із тим, які зі сценаріїв мають запускатися."

#: ../../reference_manual/preferences/python_plugin_manager.rst:27
msgid ""
"You can use the checkboxes to toggle them. A restart is required to complete "
"switching off or on the python plugin."
msgstr ""
"Для вмикання або вимикання додатків слід скористатися відповідним полем для "
"позначки. Для завершення процедури вимикання або вмикання додатка Python "
"слід перезапустити програму."

#: ../../reference_manual/preferences/python_plugin_manager.rst:29
msgid ""
"If you |mouseleft| a plugin, and the plugin has a manual, Krita will display "
"it in the box at the bottom."
msgstr ""
"Якщо ви клацнете |mouseleft| на пункті додатка і для додатка передбачено "
"підручник, Krita покаже його на панелі, розташованій у нижній частині вікна."

#: ../../reference_manual/preferences/python_plugin_manager.rst:31
msgid ""
"For more information on python, check the :ref:`python scripting category "
"<python_scripting_category>`."
msgstr ""
"Щоб дізнатися більше про додатки мовою Python, ознайомтеся із розділами :ref:"
"`категорії сценаріїв мовою Python <python_scripting_category>`."
