msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___blending_modes___hsx.pot\n"

#: ../../reference_manual/blending_modes/hsx.rst:1
msgid ""
"Page about the HSX blending modes in Krita, amongst which Hue, Color, "
"Luminosity and Saturation."
msgstr "介绍 Krita 的各种 HSX 混色模式，包括：色相、颜色、光度、饱和度等。"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:202
msgid "Intensity"
msgstr "强度"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:214
msgid "Value"
msgstr "明度"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:226
msgid "Lightness"
msgstr "亮度"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:238
msgid "Luminosity"
msgstr "光度"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Hue"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Saturation"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Luma"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Brightness"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:15
msgid "HSX"
msgstr "HSX"

#: ../../reference_manual/blending_modes/hsx.rst:17
msgid ""
"Krita has four different HSX coordinate systems. The difference between them "
"is how they handle tone."
msgstr "Krita 有四种不同的 HSX 坐标系统，它们使用不同的方式来处理颜色的亮度。"

#: ../../reference_manual/blending_modes/hsx.rst:20
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/blending_modes/hsx.rst:22
msgid ""
"HSI is a color coordinate system, using Hue, Saturation and Intensity to "
"categorize a color. Hue is roughly the wavelength, whether the color is red, "
"yellow, green, cyan, blue or purple. It is measure in 360°, with 0 being "
"red. Saturation is the measurement of how close a color is to gray. "
"Intensity, in this case is the tone of the color. What makes intensity "
"special is that it recognizes yellow (rgb:1,1,0) having a higher combined "
"rgb value than blue (rgb:0,0,1). This is a non-linear tone dimension, which "
"means it's gamma-corrected."
msgstr ""
"HSI 颜色坐标系统通过色相 (Hue)、饱和度 (Saturation)、强度 (Intensity) 来对颜"
"色进行定义。色相大致上就是色光的波长，决定了颜色是红、黄、绿、青、蓝、紫。它"
"把颜色数值分布在色相环的 360° 上，其中 0° 为红。饱和度是颜色与灰的接近程度的"
"指标。强度是颜色的亮度，它把黄 (rgb:1,1,0) 视作比蓝 (rgb:0,0,1) 的合并亮度更"
"高。它是一个 gamma 修正的非线性亮度空间。"

#: ../../reference_manual/blending_modes/hsx.rst:28
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/blending_modes/hsx.rst:30
msgid ""
"HSL is also a color coordinate system. It describes colors in Hue, "
"Saturation and Lightness. Lightness specifically puts both yellow "
"(rgb:1,1,0), blue (rgb:0,0,1) and middle gray (rgb:0.5,0.5,0.5) at the same "
"lightness (0.5)."
msgstr ""
"HSL 颜色坐标系统通过色相 (Hue)、饱和度 (Saturation)、亮度 (Lightness) 来对颜"
"色进行定义。此系统中的黄 (rgb:1,1,0)、蓝 (rgb:0,0,1)、中间灰 "
"(rgb:0.5,0.5,0.5) 具有相等的亮度 (0.5)。"

#: ../../reference_manual/blending_modes/hsx.rst:34
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/blending_modes/hsx.rst:36
msgid ""
"HSV, occasionally called HSB, is a color coordinate system. It measures "
"colors in Hue, Saturation, and Value (also called Brightness). Value or "
"Brightness specifically refers to strength at which the pixel-lights on your "
"monitor have to shine. It sets Yellow (rgb:1,1,0), Blue (rgb:0,0,1) and "
"White (rgb:1,1,0) at the same Value (100%)."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:40
msgid "HSY"
msgstr "HSY"

#: ../../reference_manual/blending_modes/hsx.rst:42
msgid ""
"HSY is a color coordinate system. It categorizes colors in Hue, Saturation "
"and Luminosity. Well, not really, it uses Luma instead of true luminosity, "
"the difference being that Luminosity is linear while Luma is gamma-corrected "
"and just weights the rgb components. Luma is based on scientific studies of "
"how much light a color reflects in real-life. While like intensity it "
"acknowledges that yellow (rgb:1,1,0) is lighter than blue (rgb:0,0,1), it "
"also acknowledges that yellow (rgb:1,1,0) is lighter than cyan (rgb:0,1,1), "
"based on these studies."
msgstr ""
"HSY 颜色坐标系统通过色相 (Hue)、饱和度 (Saturation)、光度 (Luminosity, Y) 来"
"对颜色进行定义。但它使用的不是真正的光度，而是经过 gamma 修正，只衡量 RGB 分"
"量的 Luma 光度。Luma 光度基于在现实生活中颜色对光的反射情况，根据相关研究结"
"果，它把黄 (rgb:1,1,0) 视作比蓝 (rgb:0,0,1) 和 青 (rgb:0,1,1) 更亮。"

#: ../../reference_manual/blending_modes/hsx.rst:46
msgid "HSX Blending Modes"
msgstr "HSX 混色模式"

#: ../../reference_manual/blending_modes/hsx.rst:55
msgid "Color, HSV, HSI, HSL, HSY"
msgstr "颜色 HSV、HSI、HSL、HSY"

#: ../../reference_manual/blending_modes/hsx.rst:57
msgid ""
"This takes the Luminosity/Value/Intensity/Lightness of the colors on the "
"lower layer, and combines them with the Saturation and Hue of the upper "
"pixels. We refer to Color HSY as 'Color' in line with other applications."
msgstr ""
"把下层颜色的明度、强度、亮度、光度和上层颜色的饱和度和色相组合。我们为了和其"
"他软件的做法保持一致，把“颜色 HSY”叫做“颜色”。"

#: ../../reference_manual/blending_modes/hsx.rst:62
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:62
#: ../../reference_manual/blending_modes/hsx.rst:67
#: ../../reference_manual/blending_modes/hsx.rst:72
msgid "Left: **Normal**. Right: **Color HSI**."
msgstr "左： **正常** ； 右： **颜色 HSI** 。"

#: ../../reference_manual/blending_modes/hsx.rst:67
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:72
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid "Left: **Normal**. Right: **Color HSL**."
msgstr "左： **正常** ； 右： **颜色 HSL** 。"

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid "Left: **Normal**. Right: **Color HSV**."
msgstr "左： **正常** ； 右： **颜色 HSV** 。"

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid "Left: **Normal**. Right: **Color**."
msgstr "左： **正常** ； 右： **颜色** 。"

#: ../../reference_manual/blending_modes/hsx.rst:99
msgid "Hue HSV, HSI, HSL, HSY"
msgstr "色相 HSV、HSI、HSL、HSY"

#: ../../reference_manual/blending_modes/hsx.rst:101
msgid ""
"Takes the saturation and tone of the lower layer and combines them with the "
"hue of the upper-layer. Tone in this case being either Value, Lightness, "
"Intensity or Luminosity."
msgstr ""
"把下层颜色的饱和度和亮度与上层颜色的色相组合。此处的“亮度”对应各个模式的明"
"度、强度、亮度、光度等。我们为了和其他软件的做法保持一致，把“色相 HSY”叫做“色"
"相”。"

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid "Left: **Normal**. Right: **Hue HSI**."
msgstr "左： **正常** ； 右： **色相 HSI** 。"

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid "Left: **Normal**. Right: **Hue HSL**."
msgstr "左： **正常** ； 右： **色相 HSL** 。"

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid "Left: **Normal**. Right: **Hue HSV**."
msgstr "左： **正常** ； 右： **色相 HSV** 。"

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid "Left: **Normal**. Right: **Hue**."
msgstr "左： **正常** ； 右： **色相** 。"

#: ../../reference_manual/blending_modes/hsx.rst:134
msgid "Increase Value, Lightness, Intensity or Luminosity."
msgstr "提高明度、亮度、强度、光度"

#: ../../reference_manual/blending_modes/hsx.rst:136
msgid ""
"Similar to lighten, but specific to tone. Checks whether the upper layer's "
"pixel has a higher tone than the lower layer's pixel. If so, the tone is "
"increased, if not, the lower layer's tone is maintained."
msgstr ""
"和 :ref:`bm_lighten` 类似，但仅针对亮度。检查上层颜色是否比下层颜色更亮，如果"
"是，则提高亮度，如果否，则保持下层亮度不变。"

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid "Left: **Normal**. Right: **Increase Intensity**."
msgstr "左： **正常** ； 右： **提高强度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid "Left: **Normal**. Right: **Increase Lightness**."
msgstr "左： **正常** ； 右： **提高亮度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid "Left: **Normal**. Right: **Increase Value**."
msgstr "左： **正常** ； 右： **提高明度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid "Left: **Normal**. Right: **Increase Luminosity**."
msgstr "左： **正常** ； 右： **提高光度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:170
msgid "Increase Saturation HSI, HSV, HSL, HSY"
msgstr "提高饱和度 HSV、HSI、HSL、HSY"

#: ../../reference_manual/blending_modes/hsx.rst:172
msgid ""
"Similar to lighten, but specific to Saturation. Checks whether the upper "
"layer's pixel has a higher Saturation than the lower layer's pixel. If so, "
"the Saturation is increased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"和 :ref:`bm_lighten` 类似，但仅针对饱和度。检查上层颜色是否比下层颜色更饱和，"
"如果是，则提高饱和度，如果否，则保持下层饱和度不变。我们为了和其他软件的做法"
"保持一致，把“提高饱和度 HSY”叫做“提高饱和度”。"

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid "Left: **Normal**. Right: **Increase Saturation HSI**."
msgstr "左： **正常** ； 右： **提高饱和度 HSI** 。"

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid "Left: **Normal**. Right: **Increase Saturation HSL**."
msgstr "左： **正常** ； 右： **提高饱和度 HSL** 。"

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid "Left: **Normal**. Right: **Increase Saturation HSV**."
msgstr "左： **正常** ； 右： **提高饱和度 HSV** 。"

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid "Left: **Normal**. Right: **Increase Saturation**."
msgstr "左： **正常** ； 右： **提高饱和度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:204
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"intensity of the upper layer."
msgstr "把下层颜色数值的色相和饱和度与上层颜色数值的强度组合。"

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Intensity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid "Left: **Normal**. Right: **Intensity**."
msgstr "左： **正常** ； 右： **强度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:216
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Value of the upper layer."
msgstr "把下层颜色数值的色相和饱和度与上层颜色数值的明度组合。"

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Value_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid "Left: **Normal**. Right: **Value**."
msgstr "左： **正常** ； 右： **明度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:228
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Lightness of the upper layer."
msgstr "把下层颜色数值的色相和饱和度与上层颜色数值的亮度组合。"

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Lightness_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid "Left: **Normal**. Right: **Lightness**."
msgstr "左： **正常** ； 右： **亮度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:240
msgid ""
"As explained above, actually Luma, but called this way as it's in line with "
"the terminology in other applications. Takes the Hue and Saturation of the "
"Lower layer and outputs them with the Luminosity of the upper layer. The "
"most preferred one of the four Tone blending modes, as this one gives fairly "
"intuitive results for the Tone of a hue."
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid "Left: **Normal**. Right: **Luminosity**."
msgstr "左： **正常** ； 右： **光度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:256
msgid "Saturation HSI, HSV, HSL, HSY"
msgstr "饱和度 HSI、HSV、HSL、HSY"

#: ../../reference_manual/blending_modes/hsx.rst:258
msgid ""
"Takes the Intensity and Hue of the lower layer, and outputs them with the "
"HSI saturation of the upper layer."
msgstr ""
"把下层颜色数值的强度/亮度/明度/光度还有色相，与上层颜色数值的饱和度组合。"

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid "Left: **Normal**. Right: **Saturation HSI**."
msgstr "左： **正常** ； 右： **饱和度 HSI** 。"

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid "Left: **Normal**. Right: **Saturation HSL**."
msgstr "左： **正常** ； 右： **饱和度 HSL** 。"

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid "Left: **Normal**. Right: **Saturation HSV**."
msgstr "左： **正常** ； 右： **饱和度 HSV** 。"

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid "Left: **Normal**. Right: **Saturation**."
msgstr "左： **正常** ； 右： **饱和度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:289
msgid "Decrease Value, Lightness, Intensity or Luminosity"
msgstr "降低明度、亮度、强度、光度"

#: ../../reference_manual/blending_modes/hsx.rst:291
msgid ""
"Similar to darken, but specific to tone. Checks whether the upper layer's "
"pixel has a lower tone than the lower layer's pixel. If so, the tone is "
"decreased, if not, the lower layer's tone is maintained."
msgstr ""
"和 :ref:`bm_darken` 类似，但仅针对亮度。检查上层颜色是否比下层颜色更暗，如果"
"是，则降低亮度，如果否，则保持下层亮度不变。"

#: ../../reference_manual/blending_modes/hsx.rst:297
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:297
#: ../../reference_manual/blending_modes/hsx.rst:302
#: ../../reference_manual/blending_modes/hsx.rst:307
msgid "Left: **Normal**. Right: **Decrease Intensity**."
msgstr "左： **正常** ； 右： **降低强度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:302
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:307
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid "Left: **Normal**. Right: **Decrease Lightness**."
msgstr "左： **正常** ； 右： **降低亮度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid "Left: **Normal**. Right: **Decrease Value**."
msgstr "左： **正常** ； 右： **降低明度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid "Left: **Normal**. Right: **Decrease Luminosity**."
msgstr "左： **正常** ； 右： **降低光度** 。"

#: ../../reference_manual/blending_modes/hsx.rst:334
msgid "Decrease Saturation HSI, HSV, HSL, HSY"
msgstr "降低饱和度 HSI、HSV、HSL、HSY"

#: ../../reference_manual/blending_modes/hsx.rst:336
msgid ""
"Similar to darken, but specific to Saturation. Checks whether the upper "
"layer's pixel has a lower Saturation than the lower layer's pixel. If so, "
"the Saturation is decreased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"和 :ref:`bm_darken` 类似，但仅针对饱和度。检查上层颜色是否比下层颜色更不饱"
"和，如果是，则降低不饱和度，如果否，则保持下层饱和度不变。我们为了和其他软件"
"的做法保持一致，把“降低饱和度 HSY”叫做“降低饱和度”。"

#: ../../reference_manual/blending_modes/hsx.rst:342
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:342
#: ../../reference_manual/blending_modes/hsx.rst:347
#: ../../reference_manual/blending_modes/hsx.rst:352
msgid "Left: **Normal**. Right: **Decrease Saturation HSI**."
msgstr "左： **正常** ； 右： **降低饱和度 HSI** 。"

#: ../../reference_manual/blending_modes/hsx.rst:347
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:352
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid "Left: **Normal**. Right: **Decrease Saturation HSL**."
msgstr "左： **正常** ； 右： **降低饱和度 HSL** 。"

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid "Left: **Normal**. Right: **Decrease Saturation HSV**."
msgstr "左： **正常** ； 右： **降低饱和度 HSV** 。"

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid "Left: **Normal**. Right: **Decrease Saturation**."
msgstr "左： **正常** ； 右： **降低饱和度** 。"
