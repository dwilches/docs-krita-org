msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___selections.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: 矩形选区工具"

#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: 椭圆选区工具"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: 多边形选区工具"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: 路径选区工具"

#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: 轮廓选区工具"

#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: 相邻选区工具"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: 相似颜色选区工具"

#: ../../user_manual/selections.rst:1
msgid "How selections work in Krita."
msgstr "介绍 Krita 的选区工作方式。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`rectangle_selection_tool`"
msgstr ":ref:`rectangle_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a square."
msgstr "建立一个矩形选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`ellipse_selection_tool`"
msgstr ":ref:`ellipse_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectellipse|"
msgstr "|toolselectellipse|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a circle."
msgstr "建立一个椭圆形的选区。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`polygonal_selection_tool`"
msgstr ":ref:`polygonal_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../user_manual/selections.rst:1
msgid ""
"Click where you want each point of the Polygon to be. Double click to end "
"your polygon and finalize your selection area. Use the :kbd:`Shift + Z` "
"shortcut to undo last point."
msgstr ""

#: ../../user_manual/selections.rst:1
msgid ":ref:`outline_selection_tool`"
msgstr ":ref:`outline_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectoutline|"
msgstr "|toolselectoutline|"

#: ../../user_manual/selections.rst:1
msgid ""
"Outline/Lasso tool is used for a rough selection by drawing the outline."
msgstr "通过手绘轮廓建立一个选区。某些软件把它称作“套索工具”。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`similar_selection_tool`"
msgstr ":ref:`similar_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../user_manual/selections.rst:1
msgid "Similar Color Selection Tool."
msgstr "选中与点击位置颜色相似的区域。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`contiguous_selection_tool`"
msgstr ":ref:`contiguous_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../user_manual/selections.rst:1
msgid ""
"Contiguous or “Magic Wand” selects a field of color. Adjust the :guilabel:"
"`Fuzziness` to allow more changes in the field of color, by default limited "
"to the current layer."
msgstr ""
"寻找与点击位置相连的颜色区域建立选区，默认仅对当前图层进行采样。使用 :"
"guilabel:`模糊度` 选项可以羽化选区边缘。某些软件称作“魔棒”或者“连续选区”工"
"具。"

#: ../../user_manual/selections.rst:1
msgid ":ref:`path_selection_tool`"
msgstr ":ref:`path_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../user_manual/selections.rst:1
msgid ""
"Path select an area based on a vector path, click to get sharp corners or "
"drag to get flowing lines and close the path with the :kbd:`Enter` key or "
"connecting back to the first point."
msgstr ""

#: ../../user_manual/selections.rst:12
msgid "Selection"
msgstr ""

#: ../../user_manual/selections.rst:17
msgid "Selections"
msgstr "选区"

#: ../../user_manual/selections.rst:19
msgid ""
"Selections allow you to pick a specific area of your artwork to change. This "
"is useful for when you want to move a section, transform it, or paint on it "
"without affecting the other sections. There are many selection tools "
"available that select in different ways. Once an area is selected, most "
"tools will stay inside that area. On that area you can draw or use gradients "
"to quickly get colored and/or shaded shapes with hard edges."
msgstr ""

#: ../../user_manual/selections.rst:22
msgid "Creating Selections"
msgstr "创建选区"

#: ../../user_manual/selections.rst:24
msgid ""
"The most common selection tools all exist at the bottom of the toolbox. Each "
"tool selects things slightly differently. The links for each tool go into a "
"more detailed description of how to use it."
msgstr ""
"你可以在工具箱的最下方找到一系列常用的选区工具。每种选区工具通过不同的机制建"
"立选区。如需了解每种工具的具体情况，可点击下方表格中的选区工具名称。"

#: ../../user_manual/selections.rst:38
msgid ""
"You can also use the transform tools on your selection, a great way to try "
"different proportions on parts of your image."
msgstr "你还可以对选区内容使用变形工具，这在调整画面不同部分的比例时非常方便。"

#: ../../user_manual/selections.rst:41
msgid "Editing Selections"
msgstr "编辑选区"

#: ../../user_manual/selections.rst:43
msgid ""
"The tool options for each selection tool gives you the ability to modify "
"your selection."
msgstr ""
"如需对已有选区面积进行增减，可在各个选区工具的工具选项中找到对应的按钮。你还"
"可以使用下列快捷键来增减选区面积："

#: ../../user_manual/selections.rst:47
msgid "Action"
msgstr "操作"

#: ../../user_manual/selections.rst:47
msgid "Modifier"
msgstr "修饰键 (临时切换)"

#: ../../user_manual/selections.rst:47
msgid "Shortcut"
msgstr "快捷键 (永久切换)"

#: ../../user_manual/selections.rst:47
msgid "Description"
msgstr ""

#: ../../user_manual/selections.rst:49
msgid "Replace"
msgstr "替换"

#: ../../user_manual/selections.rst:49
msgid "Ctrl"
msgstr "Ctrl"

#: ../../user_manual/selections.rst:49
msgid "R"
msgstr "R"

#: ../../user_manual/selections.rst:49
msgid "Replace the current selection."
msgstr ""

#: ../../user_manual/selections.rst:51
msgid "Intersect"
msgstr "相交"

#: ../../user_manual/selections.rst:51
msgid "Shift + Alt"
msgstr "Shift + Alt"

#: ../../user_manual/selections.rst:51 ../../user_manual/selections.rst:57
msgid "--"
msgstr "无"

#: ../../user_manual/selections.rst:51
msgid "Get the overlapping section of both selections"
msgstr ""

#: ../../user_manual/selections.rst:53
msgid "Add"
msgstr "添加"

#: ../../user_manual/selections.rst:53
msgid "Shift"
msgstr "Shift"

#: ../../user_manual/selections.rst:53
msgid "A"
msgstr "A"

#: ../../user_manual/selections.rst:53
msgid "Add the new selection to the current selection."
msgstr ""

#: ../../user_manual/selections.rst:55
msgid "Subtract"
msgstr "减去"

#: ../../user_manual/selections.rst:55
msgid "Alt"
msgstr "Alt"

#: ../../user_manual/selections.rst:55
msgid "S"
msgstr "S"

#: ../../user_manual/selections.rst:55
msgid "Subtract the selection from the current selection."
msgstr ""

#: ../../user_manual/selections.rst:57
msgid "Symmetric Difference"
msgstr ""

#: ../../user_manual/selections.rst:57
msgid "Make a selection where both the new and current do not overlap."
msgstr ""

#: ../../user_manual/selections.rst:61
msgid "You can change this in :ref:`tool_options_settings`."
msgstr ""

#: ../../user_manual/selections.rst:63
msgid ""
"If you hover over a selection with a selection tool and no selection is "
"activated, you can move it. To quickly go into transform mode, |mouseright| "
"and select :guilabel:`Edit Selection`."
msgstr ""

#: ../../user_manual/selections.rst:66
msgid "Removing Selections"
msgstr "移除选区"

#: ../../user_manual/selections.rst:68
msgid ""
"If you want to delete the entire selection, the easiest way is to deselect "
"everything. :menuselection:`Select --> Deselect`. Shortcut :kbd:`Ctrl + "
"Shift + A`."
msgstr ""
"要移除整个选区，点击菜单栏的 :menuselection:`选择 --> 取消选择` ，你也可以使"
"用快捷键 :kbd:`Ctrl + Shift + A` 来取消选区。"

#: ../../user_manual/selections.rst:71
msgid "Display Modes"
msgstr "选区显示模式"

#: ../../user_manual/selections.rst:73
msgid ""
"In the bottom left-hand corner of the status bar there is a button to toggle "
"how the selection is displayed. The two display modes are the following: "
"(Marching) Ants and Mask. The red color with Mask can be changed in the "
"preferences. You can edit the color under :menuselection:`Settings --> "
"Configure Krita --> Display --> Selection Overlay`. If there is no "
"selection, this button will not do anything."
msgstr ""
"这个按钮位于窗口的左下角。它可以切换选区的显示模式。点击按钮可以在“蚂蚁"
"线”和“蒙版”两种模式之间切换。蒙版模式默认的红色可以在菜单栏的 :menuselection:"
"`设置 --> 配置 Krita --> 显示 --> 选区蒙版颜色` 选项中更改。如果当前没有建立"
"选区，则该按钮不会有任何作用。"

#: ../../user_manual/selections.rst:77
msgid ".. image:: images/selection/Ants-displayMode.jpg"
msgstr ""

#: ../../user_manual/selections.rst:78
msgid ""
"Ants display mode (default) is best if you want to see the areas that are "
"not selected."
msgstr ""
"蚂蚁线模式 (默认)：显示选区的大致轮廓，不会干扰画面颜色，可以看清未被选中的区"
"域。"

#: ../../user_manual/selections.rst:81
msgid ".. image:: images/selection/Mask-displayMode.jpg"
msgstr ""

#: ../../user_manual/selections.rst:82
msgid ""
"Mask display mode is good if you are interested in seeing the various "
"transparency levels for your selection. For example, when you have a "
"selection with very soft edges due using feathering."
msgstr ""

#: ../../user_manual/selections.rst:86
msgid ""
"Mask mode is activated as well when a selection mask is the active layer so "
"you can see the different selection levels."
msgstr ""

#: ../../user_manual/selections.rst:89
msgid "Global Selection Mask (Painting a Selection)"
msgstr "全局选区蒙版 (绘制选区)"

#: ../../user_manual/selections.rst:91
msgid ""
"The global Selection Mask is your selection that appears on the layers "
"docker. By default, this is hidden, so you will need to make it visible via :"
"menuselection:`Select --> Show Global Selection Mask`."
msgstr ""
"此功能会把当前选区显示为图层列表中的一个蒙版图层。要显示全局选区蒙版，点击菜"
"单栏的 :menuselection:`选择 --> 显示全局选区蒙版` 。"

#: ../../user_manual/selections.rst:94
msgid ".. image:: images/selection/Global-selection-mask.jpg"
msgstr ""

#: ../../user_manual/selections.rst:95
msgid ""
"Once the global Selection Mask is shown, you will need to create a "
"selection. The benefit of using this is that you can paint your selection "
"using any of the normal painting tools, including the transform and move. "
"The information is saved as grayscale."
msgstr ""

#: ../../user_manual/selections.rst:98
msgid ""
"You can enter the global selection mask mode quickly from the selection "
"tools by doing |mouseright| and select :guilabel:`Edit Selection`."
msgstr ""

#: ../../user_manual/selections.rst:101
msgid "Selection from layer transparency"
msgstr "按照图层透明度建立选区"

#: ../../user_manual/selections.rst:104
msgid ""
"You can create a selection based on a layer's transparency by right-clicking "
"on the layer in the layer docker and selecting :guilabel:`Select Opaque` "
"from the context menu."
msgstr ""
"要选中一个图层的不透明区域，可在图层列表中右键单击该图层，在右键菜单中点击 :"
"guilabel:`选择不透明区域` 。"

#: ../../user_manual/selections.rst:108
msgid ""
"You can also do this for adding, subtracting and intersecting by going to :"
"menuselection:`Select --> Select Opaque`, where you can find specific "
"actions for each."
msgstr ""

#: ../../user_manual/selections.rst:110
msgid ""
"If you want to quickly select parts of layers, you can hold the :kbd:`Ctrl "
"+` |mouseleft| shortcut on the layer *thumbnail*. To add a selection do :kbd:"
"`Ctrl + Shift +` |mouseleft|, to remove :kbd:`Ctrl + Alt +` |mouseleft| and "
"to intersect :kbd:`Ctrl + Shift + Alt +` |mouseleft|. This works with any "
"mask that has pixel or vector data (so everything but transform masks)."
msgstr ""

#: ../../user_manual/selections.rst:114
msgid "Pixel and Vector Selection Types"
msgstr "像素选区和矢量选区"

#: ../../user_manual/selections.rst:116
msgid ""
"Vector selections allow you to modify your selection with vector anchor "
"tools. Pixel selections allow you to modify selections with pixel "
"information. They both have their benefits and disadvantages. You can "
"convert one type of selection to another."
msgstr ""
"Krita 可以建立两种类型的选区，它们是像素选区和矢量选区。像素选区通过更改像素"
"信息来控制选中区域。矢量选区通过形状编辑工具来调整选区轮廓。这两种选区各有各"
"的优点和缺点，它们也可以互相转换。"

#: ../../user_manual/selections.rst:119
msgid ".. image:: images/selection/Vector-pixel-selections.jpg"
msgstr ""

#: ../../user_manual/selections.rst:120
msgid ""
"When creating a selection, you can select what type of selection you want "
"from the Mode in the selection tool options: Pixel or Vector. By default "
"this will be a vector."
msgstr ""

#: ../../user_manual/selections.rst:122
msgid ""
"Vector selections can be modified as any other :ref:`vector shape "
"<vector_graphics>` with the :ref:`shape_selection_tool`, if you try to paint "
"on a vector selection mask it will be converted into a pixel selection. You "
"can also convert vector shapes to selection. In turn, vector selections can "
"be made from vector shapes, and vector shapes can be converted to vector "
"selections using the options in the :guilabel:`Selection` menu. Krita will "
"add a new vector layer for this shape."
msgstr ""

#: ../../user_manual/selections.rst:124
msgid ""
"One of the most common reasons to use vector selections is that they give "
"you the ability to move and transform a selection without the kind of resize "
"artifacts you get with a pixel selection. You can also use the :ref:"
"`shape_edit_tool` to change the anchor points in the selection, allowing you "
"to precisely adjust bezier curves or add corners to rectangular selections."
msgstr ""

#: ../../user_manual/selections.rst:126
msgid ""
"If you started with a pixel selection, you can still convert it to a vector "
"selection to get these benefits. Go to :menuselection:`Select --> Convert to "
"Vector Selection`."
msgstr ""
"如果你创建的是像素选区，你也可以把它转换成矢量选区来获得上述便利。要把像素选"
"区转换为矢量选区，点击菜单栏的 :menuselection:`选择 -->转换为矢量选区` 。"

#: ../../user_manual/selections.rst:130
msgid ""
"If you have multiple levels of transparency when you convert a selection to "
"vector, you will lose the semi-transparent values."
msgstr ""

#: ../../user_manual/selections.rst:133
msgid "Common Shortcuts while Using Selections"
msgstr "选区工具的常用配套快捷键"

#: ../../user_manual/selections.rst:135
msgid "Copy -- :kbd:`Ctrl + C` or :kbd:`Ctrl + Ins`"
msgstr "复制： :kbd:`Ctrl + C` 或者 :kbd:`Ctrl + Ins`"

#: ../../user_manual/selections.rst:136
msgid "Paste -- :kbd:`Ctrl + V` or :kbd:`Shift + Ins`"
msgstr "粘贴： :kbd:`Ctrl + V` 或者 :kbd:`Shift + Ins`"

#: ../../user_manual/selections.rst:137
msgid "Cut -- :kbd:`Ctrl + X`, :kbd:`Shift + Del`"
msgstr "剪切： :kbd:`Ctrl + X` 或者 :kbd:`Shift + Del`"

#: ../../user_manual/selections.rst:138
msgid "Copy From All Layers -- :kbd:`Ctrl + Shift + C`"
msgstr "从所有图层复制： :kbd:`Ctrl + Shift + C`"

#: ../../user_manual/selections.rst:139
msgid "Copy Selection to New Layer -- :kbd:`Ctrl + Alt + J`"
msgstr "把选区内容复制到新图层： :kbd:`Ctrl + Alt + J`"

#: ../../user_manual/selections.rst:140
msgid "Cut Selection to New Layer -- :kbd:`Ctrl + Shift + J`"
msgstr "把选区内容剪切到新图层： :kbd:`Ctrl + Shift + J`"

#: ../../user_manual/selections.rst:141
msgid "Display or hide selection with :kbd:`Ctrl + H`"
msgstr "切换显示/隐藏选区  --  :kbd:`Ctrl + H`"

#: ../../user_manual/selections.rst:142
msgid "Select Opaque -- :kbd:`Ctrl +` |mouseleft| on layer thumbnail."
msgstr ""

#: ../../user_manual/selections.rst:143
msgid ""
"Select Opaque (Add) -- :kbd:`Ctrl + Shift +` |mouseleft| on layer thumbnail."
msgstr ""

#: ../../user_manual/selections.rst:144
msgid ""
"Select Opaque (Subtract) -- :kbd:`Ctrl + Alt +` |mouseleft| on layer "
"thumbnail."
msgstr ""

#: ../../user_manual/selections.rst:145
msgid ""
"Select Opaque (Intersect) -- :kbd:`Ctrl + Shift + Alt +` |mouseleft| on "
"layer thumbnail."
msgstr ""
