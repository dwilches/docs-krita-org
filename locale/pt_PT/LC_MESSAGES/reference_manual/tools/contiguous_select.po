# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-17 15:08+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: contiguousselecttool icons image Kritamouseleft\n"
"X-POFile-SpellExtra: selectionsbasics kbd generalsettings\n"
"X-POFile-SpellExtra: toolselectcontiguous images alt ref mouseleft\n"
"X-POFile-SpellExtra: mouseright Kritamouseright\n"

#: ../../<generated>:1
msgid "Limit to Current Layer"
msgstr "Limitar à Camada Actual"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: ferramenta de selecção contígua"

#: ../../reference_manual/tools/contiguous_select.rst:1
msgid "Krita's contiguous selection tool reference."
msgstr "A referência da ferramenta de selecção contígua."

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Contiguous Selection"
msgstr "Selecção Contígua"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Magic Wand"
msgstr "Varinha Mágica"

#: ../../reference_manual/tools/contiguous_select.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/contiguous_select.rst:16
msgid "Contiguous Selection Tool"
msgstr "Ferramenta de Selecção Contígua"

#: ../../reference_manual/tools/contiguous_select.rst:18
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../reference_manual/tools/contiguous_select.rst:20
msgid ""
"This tool, represented by a magic wand, allows you to make :ref:"
"`selections_basics` by selecting a point of color. It will select any "
"contiguous areas of a similar color to the one you selected. You can adjust "
"the \"fuzziness\" of the tool in the tool options dock. A lower number will "
"select colors closer to the color that you chose in the first place."
msgstr ""
"Esta ferramenta, representada por uma varinha mágica, permite-lhe criar :ref:"
"`selections_basics` através da selecção de um ponto colorido. Ele irá "
"seleccionar todas as áreas contíguas com uma cor semelhante à que "
"seleccionou. Poderá ajustar a \"difusão\" da ferramenta na área de opções da "
"ferramenta. Um valor mais baixo irá seleccionar as cores mais próximas da "
"cor que escolheu em primeiro lugar."

#: ../../reference_manual/tools/contiguous_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/contiguous_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` configura a selecção para `substituição` nas opções da ferramenta; "
"este é o modo por omissão."

#: ../../reference_manual/tools/contiguous_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` configura a selecção como `adição` nas opções da ferramenta."

#: ../../reference_manual/tools/contiguous_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` configura a selecção para `subtracção` nas opções da ferramenta."

#: ../../reference_manual/tools/contiguous_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Shift` + |mouseleft| configura a selecção subsequente para `adição`. "
"Poderá largar a tecla :kbd:`Shift` enquanto arrasta, mas continuará à mesma "
"no modo de 'adição'. O mesmo se aplica aos outros."

#: ../../reference_manual/tools/contiguous_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| configura a selecção subsequente como `subtracção`."

#: ../../reference_manual/tools/contiguous_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| configura a selecção subsequente como "
"`substituição`."

#: ../../reference_manual/tools/contiguous_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt` + |mouseleft| configura a selecção subsequente como "
"`intersecção`."

#: ../../reference_manual/tools/contiguous_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr "Se passar o cursor sobre uma selecção permitir-lhe-á movê-la."

#: ../../reference_manual/tools/contiguous_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Se usar o |mouseright|, irá abrir um menu de selecção rápida que, entre "
"outras coisas, terá a possibilitar de editar a selecção."

#: ../../reference_manual/tools/contiguous_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Poderá mudar o comportamento da tecla :kbd:`Alt` para usar como alternativa "
"o :kbd:`Ctrl`, comutando o interruptor na :ref:`general_settings`."

#: ../../reference_manual/tools/contiguous_select.rst:43
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/contiguous_select.rst:45
msgid "Anti-aliasing"
msgstr "Suavização"

#: ../../reference_manual/tools/contiguous_select.rst:46
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Isto indica se são usados contornos leves nas selecções. Algumas pessoas "
"preferem arestas vincadas para as suas selecções."

#: ../../reference_manual/tools/contiguous_select.rst:47
msgid "Fuzziness"
msgstr "Difusão"

#: ../../reference_manual/tools/contiguous_select.rst:48
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
"Isto controla se a selecção contígua vê outra cor como uma aresta ou "
"contorno."

#: ../../reference_manual/tools/contiguous_select.rst:49
msgid "Grow/Shrink selection."
msgstr "Aumentar/diminuir a selecção."

#: ../../reference_manual/tools/contiguous_select.rst:50
msgid "This value extends/contracts the shape beyond its initial size."
msgstr "Este valor expande/contrai a forma para além do seu tamanho inicial."

#: ../../reference_manual/tools/contiguous_select.rst:51
msgid "Feathering"
msgstr "Suavização"

#: ../../reference_manual/tools/contiguous_select.rst:52
msgid "This value will add a soft border to the selection-shape."
msgstr "Este valor irá adicionar um contorno suavizado à forma da selecção."

#: ../../reference_manual/tools/contiguous_select.rst:54
msgid ""
"Activating this will prevent the fill tool from taking other layers into "
"account."
msgstr ""
"Se activar isto, irá impedir a ferramenta de preenchimento de ter as outras "
"camadas em conta."
