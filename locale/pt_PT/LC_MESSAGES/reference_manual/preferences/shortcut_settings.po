# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 13:57+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Photoshop en kritashortcutsrc Krita menuselection\n"
"X-POFile-SpellExtra: image canvasinputsettings Paint\n"
"X-POFile-SpellExtra: KritaConfigureShortcuts images ref Tool kbd\n"
"X-POFile-SpellExtra: preferences\n"

#: ../../<generated>:1
msgid "Saving, loading and sharing custom shortcuts"
msgstr "Gravação, carregamento e partilha de atalhos personalizados"

#: ../../reference_manual/preferences/shortcut_settings.rst:1
msgid "Configuring shortcuts in Krita."
msgstr "Configuração dos atalhos no Krita."

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Shortcuts"
msgstr "Atalhos"

#: ../../reference_manual/preferences/shortcut_settings.rst:16
msgid "Shortcut Settings"
msgstr "Configuração dos Atalhos"

#: ../../reference_manual/preferences/shortcut_settings.rst:18
msgid ""
"Most of Krita's shortcuts are configured in the menu section :menuselection:"
"`Settings --> Configure Krita --> Configure Shortcuts`. The shortcuts "
"configured here are simple key combinations, for example the :kbd:`Ctrl + X` "
"shortcut to cut. Shortcuts can also be sequences of key combinations (e.g. :"
"kbd:`Shift + S` shortcut then the :kbd:`B` key). Krita also has a special "
"interface for configuring the mouse and stylus events sent to the canvas, "
"found under :ref:`canvas_input_settings`."
msgstr ""
"A maioria dos atalhos do Krita é configurada na secção do menu :"
"menuselection:`Configuração --> Configurar o Krita --> Configurar os "
"Atalhos`. Os atalhos aqui configurados são combinações de teclas simples "
"como, por exemplo, o :kbd:`Ctrl + X` para cortar. Os atalhos também poderão "
"ser sequências de combinações de teclas (p.ex., :kbd:`Shift + S` e depois :"
"kbd:`B`). O Krita tem também uma interface especial para configurar os "
"eventos do rato e do lápis enviados para a área de desenho, que está "
"disponível em :ref:`canvas_input_settings`."

#: ../../reference_manual/preferences/shortcut_settings.rst:21
msgid "Menu Items"
msgstr "Opções do Menu"

#: ../../reference_manual/preferences/shortcut_settings.rst:23
msgid "Search bar"
msgstr "Barra de pesquisa"

#: ../../reference_manual/preferences/shortcut_settings.rst:24
msgid ""
"Entering text here will search for matching shortcuts in the shortcut list."
msgstr ""
"Se introduzir texto aqui, irá procurar por atalhos correspondentes na lista "
"de atalhos."

#: ../../reference_manual/preferences/shortcut_settings.rst:25
msgid "Shortcut List"
msgstr "Lista de Atalhos"

#: ../../reference_manual/preferences/shortcut_settings.rst:26
msgid ""
"Shortcuts are organized into sections. Each shortcut can be given a primary "
"and alternate key combination"
msgstr ""
"Os atalhos estão organizados em secções. Cada atalho poderá ter uma "
"combinação de teclas primária e outra alternativa"

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid "Load/Save Shortcuts Profiles"
msgstr "Carregar/Gravar Perfis de Atalhos"

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid ""
"The bottom row of buttons contains commands for exporting and import "
"keyboard shortcuts."
msgstr ""
"A fila de botões inferior contém comandos para exportar e importar os "
"atalhos de teclado."

#: ../../reference_manual/preferences/shortcut_settings.rst:31
msgid ".. image:: images/preferences/Krita_Configure_Shortcuts.png"
msgstr ".. image:: images/preferences/Krita_Configure_Shortcuts.png"

#: ../../reference_manual/preferences/shortcut_settings.rst:33
msgid "Configuration"
msgstr "Configuração"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid "Primary and alternate shortcuts"
msgstr "Atalhos primários e alternativos"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid ""
"Each shortcut is assigned a default, which may be empty. The user can assign "
"up to two custom shortcuts, known as primary and alternate shortcuts. Simply "
"click on a \"Custom\" button and type the key combination you wish to assign "
"to the shortcut. If the key combination is already in use for another "
"shortcut, the dialog will prompt the user to resolve the conflict."
msgstr ""
"Cada atalho tem uma combinação predefinida, que poderá estar vazia. O "
"utilizador poderá atribuir até dois atalhos personalizados, conhecidos por "
"atalho primário e alternativo. Basta carregar num botão \"Personalizado\" e "
"replicar a combinação de teclas que deseja atribuir ao atalho. Se a "
"combinação de teclas já estiver a ser usada para outro atalho, a janela irá "
"pedir ao utilizador para resolver o conflito."

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid "Shortcut schemes"
msgstr "Esquemas de atalhos"

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid ""
"Many users migrate to Krita from other tools with different default "
"shortcuts. Krita users may change the default shortcuts to mimic these other "
"programs.  Currently, Krita ships with defaults for Photoshop and Paint Tool "
"Sai. Additional shortcut schemes can be placed in the ~/.config/krita/input/ "
"folder."
msgstr ""
"Muitos dos utilizadores mudam para o Krita a partir de outras ferramentas "
"com combinações de teclas diferentes. Os utilizadores do Krita podem mudar "
"os atalhos predefinidos para imitar esses programas. De momento, o Krita vem "
"com o suporte para os atalhos do Photoshop e do Paint Tool Sai. Poderá "
"colocar esquemas de atalhos adicionais na pasta '~/.config/krita/input/'."

#: ../../reference_manual/preferences/shortcut_settings.rst:42
msgid ""
"Users may wish to export their shortcuts to use across machines, or even "
"share with other users. This can be done with the save/load drop-down. Note: "
"the shortcuts can be saved and overridden manually by backingup the text "
"file kritashortcutsrc located in ~/.config/krita/.  Additionally, the user "
"can export a custom shortcut scheme file generated by merging the existing "
"scheme defaults with the current customizations."
msgstr ""
"Os utilizadores poderão querer exportar os atalhos deles para usar noutras "
"máquinas, ou mesmo para partilhar com outros utilizadores. Isto poderá ser "
"feito com o menu para gravar/carregar. Nota: os atalhos poderão ser gravados "
"e substituídos manualmente se substituir o ficheiro de texto "
"'kritashortcutsrc', que se localiza em ~/.config/krita/. Para além disso, o "
"utilizador poderá exportar um ficheiro com um esquema personalizado de "
"atalhos que tenha sido gerado através da reunião das predefinições do "
"esquema existente com as personalizações actuais."
