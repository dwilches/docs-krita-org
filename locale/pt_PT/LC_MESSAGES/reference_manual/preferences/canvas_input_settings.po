msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 14:24+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "Perfil"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr "Configuração da introdução de dados na área de desenho no Krita."

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "Configuração da Interacção da Área de Desenho"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr "Tablete"

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""
"O Krita tem formas de definir combinações do rato e do teclado para "
"diferentes acções. O utilizador poderá definir quais as combinações a usar "
"para um dado comando do Krita aqui. Esta secção está em desenvolvimento e "
"irá incluir mais opções no futuro."

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
"O utilizador poderá criar diferentes perfis de combinações e gravá-los."
