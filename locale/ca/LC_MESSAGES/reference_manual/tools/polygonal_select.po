# Translation of docs_krita_org_reference_manual___tools___polygonal_select.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:06+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "Antialiàsing"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: eina de selecció poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:1
msgid "Krita's polygonal selection tool reference."
msgstr "Referència de l'eina Selecció poligonal del Krita."

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygon"
msgstr "Polígon"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Selection"
msgstr "Selecció"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygonal Selection"
msgstr "Selecció poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:16
msgid "Polygonal Selection Tool"
msgstr "Eina de selecció poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:18
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../reference_manual/tools/polygonal_select.rst:20
msgid ""
"This tool, represented by a polygon with a dashed border, allows you to "
"make :ref:`selections_basics` of a polygonal area point by point. Click "
"where you want each point of the Polygon to be. Double click to end your "
"polygon and finalize your selection area."
msgstr ""
"Aquesta eina està representada per un polígon amb una vora discontinua, "
"permet fer :ref:`selections_basics` d'una àrea poligonal punt per punt. Feu "
"clic on vulgueu de cada punt del Polígon. Feu doble clic per a finalitzar el "
"polígon i finalitzar la vostra àrea de selecció."

#: ../../reference_manual/tools/polygonal_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Dreceres i tecles apegaloses"

#: ../../reference_manual/tools/polygonal_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` estableix la selecció a «Substitueix» a les Opcions de l'eina, és "
"el mode predeterminat."

#: ../../reference_manual/tools/polygonal_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` estableix la selecció a «Afegeix» a les Opcions de l'eina."

#: ../../reference_manual/tools/polygonal_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ":kbd:`S` estableix la selecció a «Sostreu» a les Opcions de l'eina."

#: ../../reference_manual/tools/polygonal_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Majús. + feu` |mouseleft| estableix la selecció subsegüent a "
"«Afegeix». Podeu alliberar la tecla :kbd:`Majús.` mentre s'arrossega, però "
"encara serà establerta a «Afegeix». El mateix per a les altres."

#: ../../reference_manual/tools/polygonal_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt + feu` |mouseleft| estableix la selecció subsegüent a «Sostreu»."

#: ../../reference_manual/tools/polygonal_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl + feu` |mouseleft| estableix la selecció subsegüent a "
"«Substitueix»."

#: ../../reference_manual/tools/polygonal_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Majús. + Alt + feu` |mouseleft| estableix la selecció subsegüent a "
"«Interseca»."

#: ../../reference_manual/tools/polygonal_select.rst:36
msgid "Hovering over a selection allows you to move it."
msgstr "Passar el cursor sobre una selecció permet moure-la."

#: ../../reference_manual/tools/polygonal_select.rst:37
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Fer |mouseright| obrirà un menú ràpid de selecció amb la possibilitat "
"d'editar la selecció, entre d'altres."

#: ../../reference_manual/tools/polygonal_select.rst:41
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Podeu canviar el comportament de la tecla :kbd:`Alt` per utilitzar la tecla :"
"kbd:`Ctrl` en lloc d'alternar el canvi als :ref:`general_settings`."

#: ../../reference_manual/tools/polygonal_select.rst:44
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/polygonal_select.rst:47
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Alterna entre donar o no seleccions amb vores suaus. Hi ha qui prefereix "
"vores precises per a les seves seleccions."
