# Spanish translations for docs_krita_org_reference_manual___preferences___canvas_input_settings.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___canvas_input_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 19:24+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "Perfil"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr "Preferencias de entrada del lienzo de Krita."

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "Preferencias de entrada del lienzo"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
