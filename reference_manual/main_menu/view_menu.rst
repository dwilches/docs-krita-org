.. meta::
   :description:
        The view menu in Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
   :license: GNU free documentation license 1.3 or later.

.. index:: ! View, Wrap around mode
.. _view_menu:

=========
View Menu
=========

Show Canvas Only
    Only shows the canvas and what you have configured to show in :guilabel:`Canvas Only` settings.
Fullscreen mode
    This will hide the system bar.
Wrap Around Mode
    This will show the image as if tiled orthographically. Very useful for tiling 3d textures.
Instant Preview
    Toggle :ref:`instant_preview` globally.
Soft Proofing
    Activate :ref:`soft_proofing`.
Out of Gamut Warnings
    See the :ref:`soft_proofing` page for details.
Canvas
    Contains view manipulation actions.
Mirror View
    This will mirror the view. Hit the :kbd:`M` key to quickly activate it. Very useful during painting.
Show Rulers
    This will display a set of rulers. |mouseright| the rulers after showing them, to change the units.
Rulers track pointer
    This adds a little marker to the ruler to show where the mouse is in relation to them.
Show Guides
    Show or hide the guides.
Lock Guides
    Prevent the guides from being able to be moved by the cursor.
Show Status Bar
    This will show the status bar. The status bar contains a lot of important information, a zoom widget, and the button to switch Selection Display Mode.
Show Grid
    Shows and hides the grid. Shortcut: :kbd:`Ctrl + Shift + '`  
Show Pixel Grid
    Show the pixel grid as configured in the :ref:`display_settings`.
Snapping
    Toggle the :ref:`snapping` types.
Show Painting Assistants
    Shows or hides the Assistants.
Show Painting Previews
    Shows or hides the Previews.
